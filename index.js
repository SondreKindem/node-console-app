const fs = require('fs');
const os = require('os');
const http = require('http');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

// Initially server var here is null, so we can access it from everywere
let server = null;

getInput();

function getInput() {
    console.log("\nChoose an option:\n" +
        "1. Read package.json\n" +
        "2. Dispay OS info\n" +
        "3. Start http server\n" +
        "4. Stop the http server\n");

    readline.question("What you wanna do? > ", async input => {
        if (input === "1") {
            const file = fs.readFileSync('package.json', 'utf8');
            console.log(file);
        } else if (input === "2") {
            console.log(`
                MEMORY: ${parseFloat(os.totalmem() / Math.pow(1024, 3).toFixed(2))} GiB
                CPU CORES: ${os.cpus().length}
                PLATFORM: ${os.platform()}
                USER: ${os.userInfo().username}`
            )
        } else if (input === "3") {
            if (server && server.listening) {
                console.log("Server is already started");
            } else {
                // Wait for start
                await startServer();
            }
        } else if (input === "4") {
            if (server && server.listening) {
                // Wait for stop
                await stopServer();
            } else {
                console.log("Server is not running")
            }
        } else {
            console.log("\nInvalid option, try again ;)")
        }

        getInput();
    });
}

async function startServer() {
    const hostname = '127.0.0.1';
    const port = 3000;

    server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
    });

    // Return once server is listening
    return server.listen(port, hostname, () => console.log(`Starting server at http://${hostname}:${port}/`));
}

async function stopServer() {
    console.log("Closing server...");
    // Return once server has been closed
    return await server.close(() => console.log("Server closed!"));
}
